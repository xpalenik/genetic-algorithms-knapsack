# genome - encodes a solution
# population - all solutions
# fitness function - to evaluate solutions
# select function - select 2 solutions to generate the next generation
# crossover function - get 2 new genomes from 2 existing genomes (after "crossing them over")

from collections import namedtuple
from functools import partial
from random import choices, randint, randrange, random
from typing import List, Callable, Tuple, Optional

Genome = List[int]
Population = List[Genome]
FitnessFunc = Callable[[Genome], int]
Thing = namedtuple('Thing', ['name', 'value', 'weight'])

example_things = [
    Thing('Laptop', 500, 2200),
    Thing('Headphones', 150, 160),
    Thing('Coffee_Mug', 60, 350),
    Thing('Notepad', 40, 333),
    Thing('Water_Bottle', 30, 192),
]

more_example_things = [
                          Thing('Mints', 5, 25),
                          Thing('Socks', 10, 38),
                          Thing('Tissues', 15, 80),
                          Thing('Phone', 500, 200),
                          Thing('Baseball Cap', 100, 70)
                      ] + example_things


def generate_genome(length: int) -> Genome:
    return choices([0, 1], k=length)


def generate_population(size: int, genome_length: int) -> Population:
    return [generate_genome(genome_length) for _ in range(size)]


def fitness(genome: Genome, things: List[Thing], weight_limit: int) -> int:
    if len(genome) != len(things):
        raise ValueError("genome and things must be of the same length")

    things = [thing for exists, thing in zip(genome, things) if exists]
    value = sum([thing.value for thing in things])
    weight = sum([thing.weight for thing in things])

    return value if weight <= weight_limit else 0


def selection_pair(population: Population, fitness_func: FitnessFunc) -> Population:
    return choices(
        population=population,
        weights=[fitness_func(genome) for genome in population],
        k=2
    )


def single_point_crossover(a: Genome, b: Genome) -> Tuple[Genome, Genome]:
    if len(a) != len(b):
        raise ValueError("Genomes a and b must be of same length")

    p = randint(1, len(a))
    return (a, b) if len(a) < 2 else (a[:p] + b[p:], a[p:] + b[:p])


def mutation(genome: Genome, num: int = 1, probability: float = 0.5) -> Genome:
    for _ in range(num):
        index = randrange(len(genome))
        genome[index] = genome[index] if random() < probability else genome[index] ^ 1
    return genome


def run_evolution(
        populate_func: Callable[[], Population],
        fitness_func: Callable[[Genome], int],
        fitness_limit: int,
        selection_func: Callable[[Population, FitnessFunc], Tuple[Genome, Genome]] = selection_pair,
        crossover_func: Callable[[Genome, Genome], Tuple[Genome, Genome]] = single_point_crossover,
        mutation_func: Callable[[Genome], Genome] = mutation,
        generation_limit: int = 100,
        printer: Optional[Callable[[Population, int, FitnessFunc], None]] = None
) -> Tuple[Population, int]:
    population = populate_func()
    i = 0

    for i in range(generation_limit):
        population = sorted(population, key=lambda genome: fitness_func(genome), reverse=True)

        if printer is not None:
            printer(population, i, fitness_func)

        if fitness_func(population[0]) >= fitness_limit:
            break

        next_generation = population[0:2]

        for j in range(int(len(population) / 2) - 1):
            parents = selection_func(population, fitness_func)
            offspring_a, offspring_b = crossover_func(parents[0], parents[1])
            offspring_a = mutation_func(offspring_a)
            offspring_b = mutation_func(offspring_b)
            next_generation += [offspring_a, offspring_b]

        population = next_generation

    population = sorted(population, key=lambda genome: fitness_func(genome), reverse=True)
    return population, i


population, generations = run_evolution(
    populate_func=partial(
        generate_population, size=10, genome_length=len(example_things)
    ),
    fitness_func=partial(
        fitness, things=example_things, weight_limit=3000
    ),
    fitness_limit=740,
    generation_limit=100
)


def print_things(things: List[Thing], genome: Genome):
    things_in_bag = [thing for exists, thing in zip(genome, things) if exists]
    print("total weight: ", sum([thing.weight for thing in things_in_bag]))
    print("total value: ", sum([thing.value for thing in things_in_bag]))
    print("---")
    [print(thing.name, thing.value, thing.weight) for thing in things_in_bag]



print(f"number of generations: {generations}")
print_things(example_things, population[0])
